const csv = require("csvtojson");
const fs = require("fs");

// Conversion of file from csv to json
const csvFilePath1 = "/home/vivek/cohort-15.1/vivek_ipl_project/src/data/deliveries.csv";
const csvFilePath2 = "/home/vivek/cohort-15.1/vivek_ipl_project/src/data/matches.csv";

csv()
  .fromFile(csvFilePath1)
  .then((jsonObject) => {
    fs.writeFile(
      "/home/vivek/cohort-15.1/vivek_ipl_project/src/data/deliveries.json",
      JSON.stringify(jsonObject),
      (err) => {
        console.log(err);
      }
    );
  });

  csv()
  .fromFile(csvFilePath2)
  .then((jsonObject) => {
    fs.writeFile(
      "/home/vivek/cohort-15.1/vivek_ipl_project/src/data/matches.json",
      JSON.stringify(jsonObject),
      (err) => {
        console.log(err);
      }
    );
  });
