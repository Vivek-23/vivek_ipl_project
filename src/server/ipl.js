const fs = require('fs');

const bowlerBestEconomySuperOver = require('./ipl/bowlerEconomySuperOver');
const getExtraRunsConcededPerTeam = require('./ipl/extraRunsConcededPerTeam');
const playerWonHighestAwardEachSeason = require('./ipl/highestAwardEachSeason');
const highestNumberPlayerDismissed = require('./ipl/highestPlayerDismissed');
const getNumberOfMatchesWonPerTeamPerYear = require('./ipl/matchesWonPerTeamPerYear');
const getNumberOfMatchedPlayedPerYear = require("./ipl/matchPlayedPerYear");
const strikeRateBatsmanEachSeason = require('./ipl/strikerateBatsman');
const numberOfTimesTeamWonTheMatch = require('./ipl/teamsWonMatch');
const topEconomicalBowlers = require('./ipl/topEconomicalBowlers');



const delivery = JSON.parse(fs.readFileSync('../data/deliveries.json', 'utf-8'));
const match = JSON.parse(fs.readFileSync('../data/matches.json', 'utf-8'));

const fileOutputPath = '/home/vivek/cohort-15.1/vivek_ipl_project/src/public/output/'

//console.log(getNumberOfMatchedPlayedPerYear(match));
fs.writeFileSync(
    fileOutputPath + 'getNumberOfMatchedPlayedPerYear.json',
    JSON.stringify(getNumberOfMatchedPlayedPerYear(match), null, 2)
);

//console.log(getNumberOfMatchesWonPerTeamPerYear(match))
fs.writeFileSync(
    fileOutputPath + 'getNumberOfMatchesWonPerTeamPerYear.json',
    JSON.stringify(getNumberOfMatchesWonPerTeamPerYear(match), null, 2)
);

//console.log(getExtraRunsConcededPerTeam(match,delivery));
fs.writeFileSync(
    fileOutputPath + 'getExtraRunsConcededPerTeam.json',
    JSON.stringify(getExtraRunsConcededPerTeam(match, delivery), null, 2)
);

//console.log(topEconomicalBowlers(match, delivery))
fs.writeFileSync(
    fileOutputPath + 'topEconomicalBowlers.json',
    JSON.stringify(topEconomicalBowlers(match, delivery), null, 2)
);

//console.log(numberOfTimesTeamWonTheMatch(match));
fs.writeFileSync(
    fileOutputPath + 'numberOfTimesTeamWonTheMatch.json',
    JSON.stringify(numberOfTimesTeamWonTheMatch(match), null, 2)
);

//console.log(playerWonHighestAwardEachSeason(match));
fs.writeFileSync(
    fileOutputPath + 'playerWonHighestAwardEachSeason.json',
    JSON.stringify(playerWonHighestAwardEachSeason(match), null, 2)
);

//console.log(strikeRateBatsmanEachSeason(match, delivery))
fs.writeFileSync(
    fileOutputPath + 'strikeRateBatsmanEachSeason.json',
    JSON.stringify(strikeRateBatsmanEachSeason(match, delivery), null, 2)
);

//console.log(highestNumberPlayerDismissed(delivery));
fs.writeFileSync(
    fileOutputPath + 'highestNumberPlayerDismissed.json',
    JSON.stringify(highestNumberPlayerDismissed(delivery), null, 2)
);

console.log(bowlerBestEconomySuperOver(delivery))
fs.writeFileSync(
    fileOutputPath + 'bowlerBestEconomySuperOver.json',
    JSON.stringify(bowlerBestEconomySuperOver(delivery), null, 2)
);