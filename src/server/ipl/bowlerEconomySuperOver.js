function bowlerBestEconomySuperOver(deliverydata) {
  let superOverDelivery = deliverydata.filter((delivery) => {
    return delivery.is_super_over == 1;
  })

  let obj = superOverDelivery.reduce((result, delivery) => {
      if (result.hasOwnProperty(delivery.bowler)) {
        result[delivery.bowler]["runs"] += parseInt(delivery.total_runs);
        result[delivery.bowler]["balls"] += 1;
      } else {
        result[delivery.bowler] = {};
        result[delivery.bowler]["runs"] = parseInt(delivery.total_runs);
        result[delivery.bowler]["balls"] = 1;
      }

      result[delivery.bowler]["economy"] = (
        result[delivery.bowler]["runs"] /
        (result[delivery.bowler]["balls"] / 6)
      ).toFixed(2);

    return result;
  }, {});

  let result = Object.entries(obj)
    .sort((playerA, playerB) => {
      /**
       Using index[1] because index[1] contains the economy value and index[0] contain
        player_name
      */
      return playerA[1].economy - playerB[1].economy;
    })
    .slice(0, 1)
    .reduce((acc, curr) => {
      /**
       * Using curr[0] contains player name and curr[1] contains economy value.
       */
      acc[curr[0]] = curr[1].economy;
      return acc;
    }, {});

  return result;
}

module.exports = bowlerBestEconomySuperOver;
