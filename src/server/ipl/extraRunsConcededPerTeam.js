function getExtraRunsConcededPerTeam(matchdata, deliverydata) {
  let deliveryData = getSpecificDataPerYear(matchdata, deliverydata, 2016);

  let result = deliveryData.reduce((result, team) => {
      if (result[team.bowling_team] === undefined) {
        result[team.bowling_team] = parseInt(team.extra_runs);
      } else {
        result[team.bowling_team] += parseInt(team.extra_runs);
      }

    return result;
  }, {});

  return result;
}

function getSpecificDataPerYear(matchdata, deliverydata, year) {
  let matchData = matchdata
    .filter((match) => match.season == year)
    .map((data) => data.id);

  let first = parseInt(matchData[0]);
  let last = parseInt(matchData[matchData.length - 1]);

  let deliveryData = deliverydata.filter((delivery) => {
    return delivery.match_id >= first && delivery.match_id <= last;
  });

  return deliveryData;
}

module.exports = getExtraRunsConcededPerTeam;
