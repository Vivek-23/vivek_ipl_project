function playerWonHighestAwardEachSeason(matchdata) {
  let seasons = [...new Set(matchdata.map((match) => {
    return match.season
  }))];
  
  let obj = {};

  seasons.forEach((season) => {
    let obj1 = matchdata.reduce((result, match) => {
      if (match.season == season) {
        if (result.hasOwnProperty(match.player_of_match)) {
          result[match.player_of_match]++;
        } else {
          result[match.player_of_match] = 1;
        }
      }
      
      return result;
    }, {});

    let result = Object.entries(obj1)
      .sort((playerA, playerB) => {
        /**
         *  Using player A or B index[1] because index[1] contains total 
         * number of player of match award for each player
         */
        return playerB[1] - playerA[1];
      })
      .slice(0, 1)
      .reduce((acc, curr) => {
        /**
         * curr[0] contains player's names and and curr[1] contains total
         * number of player of match award
         */
        acc[curr[0]] = curr[1];
        return acc;
      });

      /**
       * Using result[0] because index[0] contain player's names
       */
    obj[season] = result[0];
  });

  return obj;
}

module.exports = playerWonHighestAwardEachSeason;
