function highestNumberPlayerDismissed(deliverydata) {
  let deliveryData = deliverydata.filter((delivery) => {
    return delivery.player_dismissed != "";
  });

  let obj = deliveryData.reduce((result, delivery) => {
    if (result.hasOwnProperty(delivery.player_dismissed)) {
      result[delivery.player_dismissed]++;
    } else {
      result[delivery.player_dismissed] = 1;
    }
    return result;
  }, {});

  let result = Object.entries(obj)
    .sort((playerA, playerB) => {
      /**
       * Using playerA or playerB index[1] because index[1] contains total
       * counts of player_dismissed for each player
       */
      return playerB[1] - playerA[1];
    })
    .splice(0, 1)
    .reduce((acc, curr) => {
      /**
       * curr[0] indicates the player's name and curr[1] indicates
       * total counts of player_dismissed
       */
      acc[curr[0]] = curr[1];
      return acc;
    }, {});

  return result;
}

module.exports = highestNumberPlayerDismissed;
