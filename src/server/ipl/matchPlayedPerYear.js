function getNumberOfMatchedPlayedPerYear(matchdata) {
    let result = matchdata.reduce((matchPlayed, match) => {
      if(matchPlayed.hasOwnProperty(match.season)){
        matchPlayed[match.season]++;
      }else{
        matchPlayed[match.season] = 1;
      }
      return matchPlayed;
    }, {})
    return result;
}


module.exports = getNumberOfMatchedPlayedPerYear;