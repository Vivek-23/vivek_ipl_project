function getNumberOfMatchesWonPerTeamPerYear(matchdata) {
  let matchData = matchdata.filter((match) => {
    return match.winner != "";
  });

  let result = matchData.reduce((team, match) => {
    if (team.hasOwnProperty(match.winner)) {
      if (team[match.winner].hasOwnProperty(match.season)) {
        team[match.winner][match.season]++;
      } else {
        team[match.winner][match.season] = 1;
      }
    } else {
      team[match.winner] = {};
    }

    return team;
  }, {});
  
  return result;
}

module.exports = getNumberOfMatchesWonPerTeamPerYear;
