function strikeRateBatsmanEachSeason(matchdata, deliverydata) {
  let seasons = [
    ...new Set(
      matchdata.map((match) => {
        return match.season;
      })
    ),
  ];

  let result = {};

  seasons.forEach((season) => {
    let deliveryData = getSpecificDataPerYear(matchdata, deliverydata, season);

    let batsman = [
      ...new Set(
        deliveryData.map((data) => {
          return data.batsman;
        })
      ),
    ];

    let obj1 = {};

    batsman.forEach((data) => {
      let totalruns = 0,
        balls = 0;
        
      deliveryData.forEach((delivery) => {
        if (data == delivery.batsman) {
          totalruns += parseInt(delivery.batsman_runs);
          balls++;
        }

        let strikeRate = (totalruns / balls) * 100;
        obj1[data] = strikeRate.toFixed(2);
      });
    });

    result[season] = obj1;
  });

  return result;
}

function getSpecificDataPerYear(matchdata, deliverydata, year) {
  let matchData = matchdata
    .filter((match) => match.season == year)
    .map((data) => data.id);

  let first = parseInt(matchData[0]);
  let last = parseInt(matchData[matchData.length - 1]);

  let deliveryData = deliverydata.filter((delivery) => {
    return delivery.match_id >= first && delivery.match_id <= last;
  });

  return deliveryData;
}

module.exports = strikeRateBatsmanEachSeason;
