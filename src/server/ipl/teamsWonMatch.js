function numberOfTimesTeamWonTheMatch(matchdata) {
  let matchData = matchdata.filter((match) => {
    return match.toss_winner == match.winner;
  });

  let result = matchData.reduce((winner, match) => {
    if (winner.hasOwnProperty(match.toss_winner)) {
      winner[match.toss_winner]++;
    } else {
      winner[match.toss_winner] = 1;
    }

    return winner;
  }, {});

  return result;
}

module.exports = numberOfTimesTeamWonTheMatch;
