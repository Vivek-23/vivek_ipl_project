function topEconomicalBowlers(matchdata, deliverydata) {
  let deliveryData = getSpecificDataPerYear(matchdata, deliverydata, 2015);

  let obj = deliveryData.reduce((result, team) => {
    if (!result.hasOwnProperty(team.bowler)) {
      result[team.bowler] = {};
      result[team.bowler]["balls"] = 1;
      result[team.bowler]["runs"] = parseInt(team.total_runs);
    } else {
      result[team.bowler]["balls"]++;
      result[team.bowler]["runs"] += parseInt(team.total_runs);
    }

    result[team.bowler]["ecomomy"] = (
      result[team.bowler]["runs"] /
      (result[team.bowler]["balls"] / 6)
    ).toFixed(2);

    return result;
  }, {});

  const result = Object.entries(obj)
    .sort((playerA, playerB) => {
      /**
       * Using playerA or playerB of index[1] because index[1] contains 
       * economy of a specific player.
       */
      return playerA[1].ecomomy - playerB[1].ecomomy;
    })
    .slice(0, 10)
    .reduce((acc, curr) => {
      /**
       * curr[0] indicates the player's name and curr[1] indicates the 
       * player's economy data;
       */
      acc[curr[0]] = curr[1].ecomomy;
      
      return acc;
    }, {});

  return result;
}

function getSpecificDataPerYear(matchdata, deliverydata, year) {
  let matchData = matchdata
    .filter((match) => match.season == year)
    .map((data) => data.id);

  let first = parseInt(matchData[0]);
  let last = parseInt(matchData[matchData.length - 1]);

  let deliveryData = deliverydata.filter((delivery) => {
    return delivery.match_id >= first && delivery.match_id <= last;
  });

  return deliveryData;
}

module.exports = topEconomicalBowlers;
